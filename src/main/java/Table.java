/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Tramboliko
 */
import java.util.*;

public class Table {

    Random rd = new Random();
    private char data[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player currentPlayer, o, x, win;
    private int turnCount;

    public Table(Player o, Player x) {
        this.o = o;
        this.x = x;
        randomTurn();
        win = null;
    }

    public void randomTurn() {
        if (rd.nextInt(2) == 0) {
            currentPlayer = o;
        } else {
            currentPlayer = x;
        }
    }

    public void setRowCol(int row, int col) {
        data[row - 1][col - 1] = currentPlayer.getName();
        turnCount++;
    }

    public boolean checkInputRowCol(int row, int col) {
        if (row < 1 || row > 3 || col < 1 || col > 3) {
            System.out.println("This col/row is not available.");
            return false;
        }
        if (data[row - 1][col - 1] == 'X' || data[row - 1][col - 1] == 'O') {
            System.out.println("This col/row is already occupied.");
            return false;
        }
        return true;
    }

    public void setWinNLose() {
        if (win == x) {
            x.win();
            o.lose();
        } else if (win == o) {
            x.lose();
            o.win();
        }
    }

    public boolean checkWin() {
        for (int i = 0; i < 3; i++) {
            if (checkRow(i)) {
                return true;
            } else if (checkCol(i)) {
                return true;
            } else if (checkX1()) {
                return true;
            } else if (checkX2()) {
                return true;
            }
        }
        switchTurn();
        return false;
    }

    public boolean checkDraw() {
        if (turnCount == 9) {
            win = null;
            x.draw();
            o.draw();
            return true;
        }
        return false;
    }

    public void switchTurn() {
        if (currentPlayer == x) {
            currentPlayer = o;
        } else if (currentPlayer == o) {
            currentPlayer = x;
        }
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public char[][] getData() {
        return data;
    }

    private boolean checkRow(int row) {
        for (int i = 0; i < 3; i++) {
            if (data[row][i] != currentPlayer.getName()) {
                return false;
            }
        }
        win = currentPlayer;
        setWinNLose();
        return true;
    }

    private boolean checkCol(int col) {
        for (int i = 0; i < 3; i++) {
            if (data[i][col] != currentPlayer.getName()) {
                return false;
            }
        }
        win = currentPlayer;
        setWinNLose();
        return true;
    }

    private boolean checkX1() {
        for (int i = 0; i < 3; i++) {
            if (data[i][i] != currentPlayer.getName()) {
                return false;
            }
        }
        win = currentPlayer;
        setWinNLose();
        return true;
    }

    private boolean checkX2() {
        for (int i = 0; i < 3; i++) {
            if (data[2 - i][0 + i] != currentPlayer.getName()) {
                return false;
            }
        }
        win = currentPlayer;
        setWinNLose();
        return true;
    }

    public Player getWinner() {
        return win;
    }

}
